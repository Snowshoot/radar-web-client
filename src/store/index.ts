import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    showModal: false,
    currentUser: "",
    followed: null,
    games: [""],
    notifications: [""],
    releasing: [""],
    mostFollowed: [""],
    topRated: [""],
    developer: [""],
    publisher: [""],
    genres: [""],
    stores: [""],
    gameReviews: [""],
    userReviews: [""],
    paramObj: {
      genres: "",
      stores: "",
      tba: "",
      excludeReleased: ""
    },
    gameDetails: ""
  },
  mutations: {
    setModalState(state, response) {
      state.showModal = response;
    },
    setGames(state, response) {
      state.games = response;
    },
    setReleasing(state, response) {
      state.releasing = response;
    },
    setMostFollowed(state, response) {
      state.mostFollowed = response;
    },
    setTopRated(state, response) {
      state.topRated = response;
    },
    setCurrentProfile(state, response) {
      state.currentUser = response;
    },
    setGameDetails(state, response) {
      state.gameDetails = response;
    },
    setFollowed(state, response) {
      state.followed = response;
    },
    setDeveloper(state, response) {
      state.developer = response;
    },
    setPublisher(state, response) {
      state.publisher = response;
    },
    setGenres(state, response) {
      state.genres = response;
    },
    setStores(state, response) {
      state.stores = response;
    },
    setGameReviews(state, response) {
      state.gameReviews = response;
    },
    setUserReviews(state, response) {
      state.userReviews = response;
    },
    setParamObj(state, response) {
      state.paramObj = response;
    },
    setNotifications(state, response) {
      state.notifications = response;
    }
  },
  actions: {
    setModalState(state, response) {
      state.commit("setModalState", response);
    },
    setGames(state, response) {
      state.commit("setGames", response);
    },
    setReleasing(state, response) {
      state.commit("setReleasing", response);
    },
    setMostFollowed(state, response) {
      state.commit("setMostFollowed", response);
    },
    setTopRated(state, response) {
      state.commit("setTopRated", response);
    },
    setCurrentUser(state, response) {
      state.commit("setCurrentProfile", response);
    },
    setGameDetails(state, response) {
      state.commit("setGameDetails", response);
    },
    setFollowed(state, response) {
      state.commit("setFollowed", response);
    },
    setDeveloper(state, response) {
      state.commit("setDeveloper", response);
    },
    setPublisher(state, response) {
      state.commit("setPublisher", response);
    },
    setGenres(state, response) {
      state.commit("setGenres", response);
    },
    setStores(state, response) {
      state.commit("setStores", response);
    },
    setGameReviews(state, response) {
      state.commit("setGameReviews", response);
    },
    setUserReviews(state, response) {
      state.commit("setUserReviews", response);
    },
    setParamObj(state, response) {
      state.commit("setParamObj", response);
    },
    setNotifications(state, response) {
      state.commit("setNotifications", response);
    }
  },
  modules: {},
  getters: {
    getGames: state => state.games,
    getReleasing: state => state.releasing,
    getMostFollowed: state => state.mostFollowed,
    getTopRated: state => state.topRated,
    getCurrentUser: state => state.currentUser,
    getGameDetails: state => state.gameDetails,
    getModalState: state => state.showModal,
    getFollowed: state => state.followed,
    getDeveloper: state => state.developer,
    getPublisher: state => state.publisher,
    getGenres: state => state.genres,
    getStores: state => state.stores,
    getGameReviews: state => state.gameReviews,
    getUserReviews: state => state.userReviews,
    getParamObj: state => state.paramObj,
    getNotifications: state => state.notifications
  }
});
