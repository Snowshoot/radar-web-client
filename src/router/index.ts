import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/Home.vue";
import Profile from "@/views/Profile.vue";
import Followed from "@/views/Followed.vue";
import Game from "@/views/Game.vue";
import Developer from "@/views/Developer.vue";
import Publisher from "@/views/Publisher.vue";
import Search from "@/views/Search.vue";
import Notifications from "@/views/Notifications.vue";
import store from "@/store";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
    meta: { requiresAuth: true }
  },
  {
    path: "/followed",
    name: "Followed",
    component: Followed,
    meta: { requiresAuth: true }
  },
  {
    path: "/notifications",
    name: "Notifications",
    component: Notifications,
    meta: { requiresAuth: true }
  },
  {
    path: "/game/:gameId",
    name: "Game",
    component: Game
  },
  {
    path: "/developer/:developerId",
    name: "Developer",
    component: Developer
  },
  {
    path: "/publisher/:publisherId",
    name: "Publisher",
    component: Publisher
  },
  {
    path: "/search",
    name: "Search",
    component: Search
  }
];

const router = new VueRouter({
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (Vue.$cookies.get("isUserLoggedIn") !== true) {
      store.dispatch("setModalState", true);
      next({
        path: "/"
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
