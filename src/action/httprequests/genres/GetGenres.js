import store from "@/store";

export default {
  name: "GenreList",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async getGenres() {
      const response = await fetch(`http://localhost:8080/genres/`, {
        method: "Get",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        .then(data => {
          store.dispatch("setGenres", data);
        });
    }
  }
};
