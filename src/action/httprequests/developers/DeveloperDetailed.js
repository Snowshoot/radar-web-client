import store from "@/store";

export default {
  name: "DeveloperDetailed",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async getDeveloper(id, page, pageSize) {
      const response = await fetch(
        `http://localhost:8080/developers/` +
          id +
          `?page=` +
          page +
          `&pageSize=` +
          pageSize,
        {
          method: "Get",
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
        .then(response => response.json())
        .then(data => {
          store.dispatch("setDeveloper", data);
        });
    }
  }
};
