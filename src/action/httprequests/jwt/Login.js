import store from "@/store";
export default {
  name: "LoginPostRequest",
  data() {
    return {
      response: false
    };
  },
  methods: {
    async logIn(login, password, cookies) {
      return await fetch(`http://localhost:8080/auth`, {
        method: "Post",
        credentials: "include",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ login, password })
      }).then(response => response.status);
    }
  }
};
