import CookieUtil from "@/action/CookieUtil";

export default {
  name: "Logout",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async logout(cookies) {
      return await fetch(`http://localhost:8080/blacklist`, {
        method: "Post",
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        }
      }).then(function(response) {
        if (response.status === 200 || response.status === 403) {
          CookieUtil.methods.removeCookie();
        }
      });
    }
  }
};
