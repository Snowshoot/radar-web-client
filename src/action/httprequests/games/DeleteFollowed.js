import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "DeleteFollowed",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async unfollow(gameId) {
      return await fetch(
        `http://localhost:8080/games/followed?gameId=` + gameId,
        {
          credentials: "include",
          method: "Delete",
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.status);
    }
  }
};
