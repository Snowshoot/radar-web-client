import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "AddFollowed",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async follow(gameId) {
      const response = await fetch(
        `http://localhost:8080/games/followed?gameId=` + gameId,
        {
          credentials: "include",
          method: "Put",
          headers: {
            "Content-Type": "application/json"
          }
        }
      ).then(response => {
        if (response.status === 403) {
          store.dispatch("setModalState", true);
          CookieUtil.methods.removeCookie();
        } else {
          return response;
        }
      });
    }
  }
};
