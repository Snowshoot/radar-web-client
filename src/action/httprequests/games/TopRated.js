import store from "@/store";

export default {
  name: "TopRated",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getTopRated: async function() {
      const response = await fetch(
        `http://localhost:8080/games/rating?page=1&pageSize=12`,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => response.json())
        .then(data => store.dispatch("setTopRated", data));
    }
  }
};
