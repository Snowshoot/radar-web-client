import store from "@/store";

export default {
  name: "GameDetails",
  data() {
    return {
      gameDetailsResponse: ""
    };
  },
  methods: {
    getGame: async function(id) {
      const response = await fetch(
        `http://localhost:8080/games/` + id,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => response.json())
        .then(data => store.dispatch("setGameDetails", data));
    }
  }
};
