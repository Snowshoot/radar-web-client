import store from "@/store";

export default {
  name: "ReviewsByGame",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getGameReviews: async function(gameId, page, pageSize) {
      const response = await fetch(
        `http://localhost:8080/games/` +
          gameId +
          `/reviews` +
          "?page=" +
          page +
          `&pageSize=` +
          pageSize,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => response.json())
        .then(data => store.dispatch("setGameReviews", data));
    }
  }
};
