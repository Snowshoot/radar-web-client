import store from "@/store";

export default {
  name: "ReleasingSoon",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getReleasing: async function() {
      const response = await fetch(`http://localhost:8080/games/soon?page=1&pageSize=12`, {
        method: "Get",
        headers: { "Content-Type": "application/json" }
      })
        .then(response => response.json())
        .then(data => store.dispatch("setReleasing", data));
    }
  }
};
