import store from "@/store";

export default {
  name: "GamesPaged",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getGames: async function(
      page,
      pageSize,
      query,
      genres,
      stores,
      tba,
      excludeReleased
    ) {
      const response = await fetch(
        `http://localhost:8080/games?page=` +
          page +
          "&pageSize=" +
          pageSize +
          "&query=" +
          query +
          "&genres=" +
          genres +
          "&stores=" +
          stores +
          "&tba=" +
          tba +
          "&excludeReleased=" +
          excludeReleased,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => response.json())
        .then(data => {
          store.dispatch("setGames", data);
        });
    }
  }
};
