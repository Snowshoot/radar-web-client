import store from "@/store";
export default {
  name: "PublisherDetailed",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async getPublisher(id, page, pageSize) {
      const response = await fetch(
        `http://localhost:8080/publishers/` +
        id +
        `?page=` +
        page +
        `&pageSize=` +
          pageSize,
        {
          method: "Get",
          headers: {
            "Content-Type": "application/json"
          }
        }
      )
        .then(response => response.json())
        .then(data => {
          store.dispatch("setPublisher", data);
        });
    }
  }
};
