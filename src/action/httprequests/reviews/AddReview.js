import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "AddReview",
  data() {
    return {
      response: false
    };
  },
  methods: {
    async addReview(review, stars, gameId) {
      return await fetch(`http://localhost:8080/reviews`, {
        method: "Post",
        credentials: "include",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ review, stars, gameId })
      })
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.status);
    }
  }
};
