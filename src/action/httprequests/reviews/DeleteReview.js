import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "DeleteReview",
  data() {
    return {
      response: false
    };
  },
  methods: {
    async deleteReview(gameId) {
      return await fetch(
        `http://localhost:8080/reviews` + "?gameId=" + gameId,
        {
          method: "Delete",
          credentials: "include",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.status);
    }
  }
};
