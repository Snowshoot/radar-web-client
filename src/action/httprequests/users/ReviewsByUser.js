import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "ReviewsByUser",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getUserReviews: async function(page, pageSize) {
      const response = await fetch(
        `http://localhost:8080/users/reviews` +
          `?page=` +
          page +
          `&pageSize=` +
          pageSize,
        {
          credentials: "include",
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      )
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.json())
        .then(data => store.dispatch("setUserReviews", data));
    }
  }
};
