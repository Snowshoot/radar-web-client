import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "GetNotifications",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    clearNotifications: async function() {
      return await fetch(`http://localhost:8080/users/notifications`, {
        credentials: "include",
        method: "Put",
        headers: { "Content-Type": "application/json" }
      })
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => {
          if (response.status === 200) {
            store.dispatch("setNotifications", "emptied");
          }
        });
    }
  }
};
