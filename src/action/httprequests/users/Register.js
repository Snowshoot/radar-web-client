export default {
  name: "RegisterPostRequest",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async register(login, password, email) {
      return  await fetch(`http://localhost:8080/users`, {
        method: "Post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ login, password, email })
      }).then(response => response.status);
    }
  }
};
