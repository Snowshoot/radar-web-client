import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "GetNotifications",
  data() {
    return {
      gameDetailsResponse: []
    };
  },
  methods: {
    getNotifications: async function() {
      await fetch(`http://localhost:8080/users/notifications`, {
        credentials: "include",
        method: "Get",
        headers: {
          "Content-Type": "application/json",
          "Keep-Alive": "timeout=10"
        }
      })
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.json())
        .then(data => store.dispatch("setNotifications", data));
    }
  }
};
