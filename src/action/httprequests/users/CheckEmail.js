export default {
  name: "CheckEmail",
  data() {
    return {
      profileData: ""
    };
  },
  methods: {
    async checkEmail(email) {
      return await fetch(
        `http://localhost:8080/users/checker/email?email=` + email,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" }
        }
      ).then(response => response.json());
    }
  }
};
