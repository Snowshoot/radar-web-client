export default {
  name: "CheckUsername",
  data() {
    return {
      profileData: ""
    };
  },
  methods: {
    async checkUsername(username) {
      return await fetch(
        `http://localhost:8080/users/checker/login?login=` + username,
        {
          method: "Get",
          headers: { "Content-Type": "application/json" },
        }
      ).then(response => response.json());
    }
  }
};
