import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "ChangePassword",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async changePassword(oldPassword, newPassword) {
      const response = await fetch(`http://localhost:8080/users/password`, {
        credentials: "include",
        method: "Put",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ oldPassword, newPassword })
      }).then(response => {
        if (response.status === 403) {
          store.dispatch("setModalState", true);
          CookieUtil.methods.removeCookie();
        } else {
          return response;
        }
      });
    }
  }
};
