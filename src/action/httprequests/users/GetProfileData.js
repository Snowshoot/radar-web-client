import store from "@/store";
import CookieUtil from "@/action/CookieUtil";
export default {
  name: "GetProfileData",
  data() {
    return {
      profileData: ""
    };
  },
  methods: {
    async getProfileData() {
      const response = await fetch(`http://localhost:8080/users/profile`, {
        method: "Get",
        headers: { "Content-Type": "application/json" },
        credentials: "include"
      })
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.json())
        .then(data => store.dispatch("setCurrentUser", data));
    }
  }
};
