import store from "@/store";
import CookieUtil from "@/action/CookieUtil";

export default {
  name: "ChangeEmail",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async changeEmail(oldPassword, newEmail) {
      return await fetch(`http://localhost:8080/users/email`, {
        credentials: "include",
        method: "Put",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ oldPassword, newEmail })
      })
        .then(response => {
          if (response.status === 403) {
            store.dispatch("setModalState", true);
            CookieUtil.methods.removeCookie();
          } else {
            return response;
          }
        })
        .then(response => response.status);
    }
  }
};
