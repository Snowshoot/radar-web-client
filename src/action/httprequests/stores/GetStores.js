import store from "@/store";
export default {
  name: "StoreList",
  data() {
    return {
      response: ""
    };
  },
  methods: {
    async getStores() {
      const response = await fetch(`http://localhost:8080/stores`, {
        method: "Get",
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        .then(data => {
          store.dispatch("setStores", data);
        });
    }
  }
};
