import Vue from "vue";

export default {
  name: "SetCookie",
  methods: {
    setCookie(value) {
      Vue.$cookies.set("isUserLoggedIn", value, "infinity");
    },
    removeCookie() {
      Vue.$cookies.remove("isUserLoggedIn");
    }
  }
};
